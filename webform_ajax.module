<?php

/**
 * @file
 * Webform AJAX module file.
 * Implements Drupal hooks to provided AJAX submission and page change to Webforms.
 */

/**
 * Implements hook_menu().
 */
function webform_ajax_menu() {
  $items = array();

  $items['webform_ajax/return_webform/%node'] = array(
    'title' => 'Webform AJAX callback',
    'page callback' => 'webform_ajax_confirm_return_ajax_callback',
    'page arguments' => array(2),
    'delivery callback' => 'ajax_deliver',
    'access callback' => 'node_access',
    'access arguments' => array('view', 2),
    'theme callback' => 'ajax_base_page_theme',
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_form_FORM_ID_alter().
 * With FORM_ID = webform_configure_form.
 * Add AJAX option to Webform configure form.
 */
function webform_ajax_form_webform_configure_form_alter(&$form, &$form_state) {
  $node = $form_state['build_info']['args'][0];
  $form['advanced']['webform_ajax'] = array(
    '#type' => 'checkbox',
    '#title' => t('AJAX mode'),
    '#description' => t('When set, all page changes (from pagebreak component), and webform submission will be achieved in AJAX.'),
    '#default_value' => $node->webform['webform_ajax'],
  );
  array_unshift($form['#submit'], 'webform_ajax_webform_configure_form_submit');
}

/**
 * Additional submit callback for form webform_configure_form.
 */
function webform_ajax_webform_configure_form_submit(&$form, &$form_state) {
  $form['#node']->webform['webform_ajax'] = $form_state['values']['webform_ajax'];
}

/**
 * Implements hook_form_FORM_ID_alter().
 * With FORM_ID = webform_client_form.
 * Add AJAX logic to Webform form.
 */
function webform_ajax_form_webform_client_form_alter(&$form, $form_state, $form_id) {
  $webform = $form['#node']->webform;
  if ($webform['webform_ajax']) {
    //@FIXME what if the same webform is displayed many times on the page? unique HTML id?
    $wrapper_id = 'webform-ajax-' . $form['#node']->nid;
    $form['#prefix'] = '<div id="' . $wrapper_id . '">' . (isset($form['#prefix']) ? $form['#prefix'] : '');
    $form['#suffix'] = (isset($form['#suffix']) ? $form['#suffix'] : '') . '</div>';

    foreach (array('previous', 'next', 'submit') as $button) {
      $form['actions'][$button]['#ajax'] = array(
        'callback' => 'webform_ajax_callback',
        'wrapper' => $wrapper_id,
        'progress' => array(
          'message' => '',
          'type' => 'throbber',
        ),
      );
    }
  }
}

/**
 * AJAX callback for Webform Prev/Next page and Submit buttons.
 * Returns the new computed webform, unless it has been completed.
 */
function webform_ajax_callback($form, &$form_state) {
  $output = array();
  // If user completed his submission, display webform confirmation, and AJAXify "Return to form" link.
  if (isset($form_state['webform_completed']) && $form_state['webform_completed']) {
    $wrapper_id = 'webform-ajax-' . $form['#node']->nid;
    $output = array(
      '#type' => 'markup',
      '#markup' => theme(array('webform_confirmation_' . $form['#node']->nid, 'webform_confirmation'), array('node' => $form['#node'], 'sid' => $form_state['values']['details']['sid'])),
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    );
    // Add Javascript which will AJAXify "Return to form" link in webform_confirmation.
    $ajax_setting = array('webform_ajax' => array(
      $wrapper_id => array(
        'html_id' => $wrapper_id,
        'nid' => $form['#node']->nid,
        'sid' => $form_state['values']['details']['sid'],
      ),
    ));
    drupal_add_js($ajax_setting, 'setting');
    drupal_add_js(drupal_get_path('module', 'webform_ajax') . '/js/webform_ajax.js', 'file');
  }
  else {
    $output = $form;
  }
  return $output;
}

/**
 * "Return to form" link AJAX callback.
 */
function webform_ajax_confirm_return_ajax_callback($node) {
  // Fake full page mode to force webform messages output.
  drupal_static_reset('arg');
  $real_get_q = $_GET['q'];
  $_GET['q'] = 'node/' . $node->nid;

  webform_node_view($node, 'full');

  // Restore real $_GET['q'].
  $_GET['q'] = $real_get_q;
  drupal_static_reset('arg');

  // Display the webform only if it is enabled. Otherwise, show messages.
  if ($node->content['webform']['#enabled']) {
    $output = $node->content['webform'];
  }
  else {
    $wrapper_id = 'webform-ajax-' . $node->nid;
    $output = array(
      '#type' => 'markup',
      '#markup' => t('The webform cannot be displayed.'),
      //@FIXME externalize wrapper_id creation in function.
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    );
  }
  return $output;
}